package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

import java.util.ArrayList;

public class ArraysMain {
    public static void main(String[] args) {

        //ArrayList<Pet> myPetList = new ArrayList<Pet>();

        int[] intArrary = {5, 10, 25, 34, 99};

        Pet[] petArray = new Pet[10];

        for(int i=0; i<petArray.length; ++i) {
            System.out.println(petArray[i]);
        }

        // Put a pet in the array
        Pet firstPet = new Pet();
        firstPet.setName("first com.training.pets.Pet");
        petArray[0] = firstPet;

        // Put a dragon in the array
        Dragon myDragon = new Dragon( "Spyro");
        petArray[1] = myDragon;

        for(int i=0; i<petArray.length; ++i) {
            System.out.println(petArray[i]);

            if(petArray[i] != null) {
                petArray[i].feed();
            }
        }

        petArray[0].feed();
        petArray[1].feed();

    }
}
