package com.training;

import com.training.pets.Dragon;

public class StringsMain {
    public static void main(String[] args) {

        String bigString = "start: ";

        StringBuilder stringBuilder = new StringBuilder(bigString);

        for(int i=0; i<10; i++) {
            stringBuilder.append(i);
        }

        System.out.println(stringBuilder);


        // below was just an example, String Buidler is better
        String testString = "hello" + " " + "world" + " "
                + "after" + " " + "lunch";

        System.out.println(testString);

        // trying to turn object into string representation
        Dragon stringyDragon = new Dragon("Smaug");
        System.out.println("Dragon Created: " + stringyDragon);

        // better way of returning an object, method adding to Dragon
        // class to call
        System.out.println("Dragon Created: "
                + stringyDragon.toString());
    }
}
