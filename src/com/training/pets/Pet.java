package com.training.pets;

public class Pet {

    private static int numPets = 0;

    private int numLegs;
    private String name;

    public Pet() {
        numPets++;
    }

    public void feed() {

        System.out.println("Feed generic pet some generic pet food");
    }

    public int getNumLegs() {

        return numLegs;
    }

    public void setNumLegs(int numLegs) {

        this.numLegs = numLegs;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public static int getNumPets() {
        return numPets;
    }

    //override another method of the same name
    @Override
    public String toString() {
        return "This is a dragon, with " + this.getNumLegs()
                + " it's name is " + this.getName();
    }

    public static void main(String[] args) {
        Pet myPet = new Pet();

        System.out.println(myPet.getNumLegs());
    }
}

