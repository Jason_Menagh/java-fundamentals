package com.training.pets;

public class Dragon extends Pet {

    public Dragon() {
        this.setNumLegs(4);
    }

    public Dragon(String name) {
        this();
        this.setName(name);
    }

    public void feed() {
        System.out.println("Feed the dragon locusts");
    }

    public void feed(String food) {
        System.out.println("Feed the dragon " + food);
    }

    public void breatheFire() { System.out.println("FIIIIRRRREEEEE"); }

    public String toString() {
        return "This is a dragon, with " + this.getNumLegs();
    }
}
