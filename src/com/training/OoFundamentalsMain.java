package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class OoFundamentalsMain {

    public static void main(String[] args) {

        Pet myPet = new Pet();
        System.out.println(Pet.getNumPets());

        myPet.setNumLegs(4);
        myPet.setName("Mycroft");

        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getName());
        myPet.feed();

        // -------------------------------------------

        Dragon myDragon = new Dragon("Bob");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed(  "people");
        myDragon.breatheFire();

        // -------------------------------------------

        Pet secondDragon = new Dragon();
        secondDragon.setName("Mycroft");

        System.out.println(secondDragon.getNumLegs());
        System.out.println(secondDragon.getName());
        secondDragon.feed();

        // -------------------------------------------

        Dragon usingNameConstructor = new Dragon("Stanley");

        System.out.println(usingNameConstructor.getNumLegs());
        System.out.println(usingNameConstructor.getName());
        usingNameConstructor.feed();
        usingNameConstructor.breatheFire();

    }
}
